# EDO accretion

Extended dark matter objects are a popular dark matter candidate at the high mass range that, in principle, only interacts with matter via gravity. Through the evolution of the universe, we expect EDOs to accrete matter, heating it up in the process and modifying the global recombination history. We explored the consequences of this phenomena in this paper, where the calculations where done numerically. In this repository, you can find the data and the Mathematica notebook we used.

## Usage
This repository contains:

-   **MATHEMATICA NOTEBOOK**: This is the code that can be used to place CMB bounds on EDOs with any mass function. The notebook serves as a tutorial, where we show how we obtained each of the figures in the paper.

-   **FIGURES**: Here we include the figures from the paper with better quality. 

-   **DATA**: Some of the routines we use to generate the data take long to run (aroun 2h for some of them). Therefore, we also upload the data here for anyone to use. However, this data is not exactly the one displayed in the figures, but using the mathematica notebook it can be used to produce the figure data.

## Citation
If you use this code, please cite [arXiv:2403.13072](https://arxiv.org/abs/2403.13072).

## License
This code is covered by MIT Licence
Copyright (c) 2024 Sergio Sevillano Muñoz and Djuna Croon.